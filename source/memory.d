module memory;

import std.stdio;

import global;

abstract class Memory {
  ubyte[] ram; //Make private
  /*RAM Layout:
    https://wiki.nesdev.com/w/index.php/CPU_memory_map
  */

  int count = 0x00; //For easily writing machine code to RAM byte by byte

  ubyte read(ushort a){
    return ram[a];
  }

  void writeNext(ubyte b){ //For CPU testing ONLY
    ram[count++] = b;
  }

  void write(ushort addr, ubyte b){ //To be overwritten
  } 

  void reset(){
    for(int i = 0; i < 0x07FF; i++){
      ram[i] = 0xFF;
    }
    count = 0;
  }
}

class CPUMemory : Memory {

  this(){
    ram = new ubyte[0xFFFF+1];
  }

  override ubyte read(ushort a) {
    if(a < 0x1FFF) {
      return ram[a%0x07FF];
    }
    if(a >= 0x2000 && a <= 0x3FFE ) {
      return ppu.read(a%8);
    }
    return ram[a];
  }

  override void write(ushort addr, ubyte b){ //CPU specific write with mirroring
    if(addr <= 0x1fff) { // Hack for writes to mirrored addresses
      addr = addr%0x07FF;
    }else if(addr == 0x4014) {
      ppu.oamDMA = b; //Init PPU OAM
    }else if(addr >= 0x2000 && addr <= 0x3FFF) {
      addr = addr%8;
      ppu.ppuram.write(addr, b);
      return;
    }
    ram[addr] = b;
  }
}

/+
 + PPU RAM, includes addresses mapped to cartridge CHR-ROM and nametables
+/
class PPUMemory : Memory {
  ubyte[0x100] oamP; //OAM for frame
  ubyte[0x020] oamS; //OAM for scanline
  this(){
    ram = new ubyte[0x3FFF]; //Addressable PPU space (Only 2k is ram)
  }

  override void write(ushort addr, ubyte b) {
    ram[addr] = b;
  }

  ubyte readOAMP(ushort addr) {
    return oamP[addr];
  }
  void writeOAMS(ushort addr, ubyte v) {
    oamS[addr] = v;
  }

  void clearoamS() {
    foreach(i; 0..oamS.length) {
      oamS[i] = 0xFF;
    }
  }

  void update() {
    auto chr = cart.chr; //ubyte[0xXXXX]
    writeln(chr.length);
    writeln(ram.length);
    foreach(ushort i; 0..cast(ushort)chr.length){ //Updates from chr rom (This should be pointers)
      ram[i] = chr[i];
    }
    foreach(i; 0x0..0x0f00) {
      ram[0x3000+i] = ram[0x2000+i];
    }
    foreach(i; 0x20..0x00DF) {
      ram[0x3f20+i] = ram[0x3f00+(i%0x20)];
    }
  }
}