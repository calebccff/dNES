import std.stdio;
import std.conv;
import std.array;
import core.time;
import std.format;

import dsfml.graphics;

import memory;

import global; //Using the cart to load CHR-ROM

long frames; //Frames this second
MonoTime fstart;
Duration ftime;

enum State {
  PRERENDER,
  VISIBLE,
  POSTRENDER,
  VBLANK
}
alias PRERENDER  = State.PRERENDER;
alias VISIBLE    = State.VISIBLE;
alias POSTRENDER = State.POSTRENDER;
alias VBLANK     = State.VBLANK;

class NesPPU{

  PPUMemory ppuram;

  //Misc hardware vars
    Image back;
    Image front; //__gshared?

    short scanline;    // 0-261, 0-239=visible, 240=post, 241-260=vblank, 261=pre
    short dot; //0-341
    uint fc; // frame counter
    State state; //Tracks the current "state" based on scanline

  //Background registers
    //Latches
      ubyte lta; //Low tile attributes
      ubyte hta; //High tile attributes
    //Background shift registers
      //Two bits per pixel: | 7| 6| 5| 4| 3| 2| 1| 0|
      //                    |15|14|13|12|11|10|09|08|
      ushort tileLow; //Low tile
      ushort tileHigh; //High tile
  //Sprite Data
      // ubyte y;      // Y position.
      // ubyte tile;   // Tile index. (tile number in pattern table, he pattern table selected b bit 3 of PPUCTRL) <- 8x8
      //               //             (LSB is used to select pattern table, other 7 bits select the tile)          <- 8x16
      // ubyte attr;   // Attributes. 
      // /*
      // 76543210
      // ||||||||
      // ||||||++- Palette (4 to 7) of sprite
      // |||+++--- Unimplemented
      // ||+------ Priority (0: in front of background; 1: behind background)
      // |+------- Flip sprite horizontally
      // +-------- Flip sprite vertically
      // */
      // ubyte x;      // X position.

  //Internal registers
    ushort v;  //Current VRAM address
    ushort t;  //Temporary VRAM address (Address of top left onscreen tile)
    /* layout of t and v
      yyy NN YYYYY XXXXX
      ||| || ||||| +++++-- coarse X ppuScroll
      ||| || +++++-------- coarse Y ppuScroll
      ||| ++-------------- nametable select
      +++----------------- fine Y ppuScroll
    */  
    ubyte  x;   //Fine x ppuScroll
    ubyte  w;    //Write toggle (Writing x or y ppuScroll)
    ubyte  z;    //Write toggle for PPUADDR (low/high byte)
    bool odd = true;  //Even/odd frame flag (0 is an odd number)
    bool render = true; //Rendering toggle

  //External registers (CPU communication);
  /* PPUCTRL $2000
    Misc. control flags
  */
  struct Ctrl{
    ubyte nt     = 0;  // Nametable ($2000 / $2400 / $2800 / $2C00).
    ubyte incr   = 0;  // Address increment (1 / 32).
    ubyte sprTbl = 0;  // Sprite pattern table ($0000 / $1000).
    ubyte bgTbl  = 0;  // BG pattern table ($0000 / $1000).
    ubyte sprSz  = 0;  // Sprite size (8x8 / 8x16).
    ubyte slave  = 0;  // PPU master/slave.
    ubyte nmi    = 0;  // Enable NMI.
  }
  Ctrl ctrlF; //To make accessing flags easier
  ubyte ctrlB; // The raw byte
  @property void ctrl(ubyte f) {
    
    ctrlB = f;
    
    ctrlF.nt       = f&0b00000011;
    ctrlF.incr     = f&0b00000100 >> 2;
    ctrlF.sprTbl   = f&0b00001000 >> 3;
    
    ctrlF.bgTbl    = f&0b00010000 >> 4;
    ctrlF.sprSz    = f&0b00100000 >> 5;
    ctrlF.slave    = f&0b01000000 >> 6;
    
    ctrlF.nmi      = f&0b10000000 >> 7;

    t = cast(ushort)(f << 11);
  }

  /* PPUMASK $2001
    Controls rendering of sprites and backgrounds
  */
  struct Mask{
    ubyte gray    = 1;  // Grayscale.
    ubyte bgLeft  = 1;  // Show background in leftmost 8 pixels.
    ubyte sprLeft = 1;  // Show sprite in leftmost 8 pixels.
    ubyte bg      = 1;  // Show background.
    ubyte spr     = 1;  // Show sprites.
    ubyte red     = 1;  // Intensify reds.
    ubyte green   = 1;  // Intensify greens.
    ubyte blue    = 1;  // Intensify blues.
  }
  Mask maskF;
  ubyte maskB;
  @property void mask(ubyte f) {
    maskB = f;
    maskF.gray     = f&0b00000001 >> 0;
    maskF.bgLeft   = f&0b00000010 >> 1;
    maskF.sprLeft  = f&0b00000100 >> 2;
    maskF.bg       = f&0b00001000 >> 3;
    maskF.spr      = f&0b00010000 >> 4;
    maskF.red      = f&0b00100000 >> 5;
    maskF.green    = f&0b01000000 >> 6;
    maskF.blue     = f&0b10000000 >> 7;
  }
  @property ubyte mask() {
    return maskB;
  }

  /* PPUSTATUS $2002
    Reflects state of various internal functions
  */
  struct Status {
    ubyte bus    = 0b00000;  // Not significant. (LS 5 bits from bus)
    ubyte sprOvf = 1;  // Sprite overflow?
    ubyte sprHit = 0;  // Sprite 0 Hit?
    ubyte vBlank = 1;  // In VBlank?
  }
  Status statusF;
  ubyte statusB;
  @property ubyte status() { //read
    w = 0x00;
    statusB = 0x0;
    statusB |= statusF.bus;
    statusB |= statusF.sprOvf << 5;
    statusB |= statusF.sprHit << 6;
    statusB |= statusF.vBlank << 7;
    statusF.vBlank = 0;
    //writeln(statusF);
    return statusB;
  }

  /* OAM Address $2003
    CPU writes address desire OAM address here, most games use OAMDMA
  */
  ubyte oamAddr;

  /* OAM Data $2004
    CPU writes 1 byte of OAM data here (Written to oamAddr)
  */
  @property oamData(ubyte v) {
    ppuram.write(oamAddr, v);
  }
  @property ubyte oamData() {
    if(dot > 0 && dot < 64) { //Can't read during secondary oam clear
      return 0xFF;
    }
    return ppuram.read(oamAddr);
  }

  /* PPUSCROLL $2005
    CPU writes x/y scroll here, two writes, first is x second is y
  */
  @property void ppuScroll(ubyte f) {
    switch(w) {
      case 0:
        t = f >> 3;
        x = f&0b00000111;
        break;
      case 1:
        t |= f << 12 + (f&0b11111000) << 2;
        break;
      default:
        //Something went badly wrong
        break;
    }
    w = w^1; //Next write will be the other coordinate
  }

  /* PPUADDR $2006
    PPU read/write address (two writes: most significant byte, least significant byte) 
  */
  @property void ppuAddr(ubyte f) {
    switch(z) {
      case 0:
        t = (f&0b0011111) << 8; //Most significant byte (removes last 2 bits)
        break;
      case 1:
        t |= f;
        v = t; //Sets actual VRAM addr to temp VRAM addr
        break;
      default:
        //Something went badly wrong
        break;
    }
    z = z^1;
  }
  @property ushort ppuAddr() { //For debugging - returns the write address
    return v;
  }

  /* PPUDATA $2007
    PPU data read/write - the data to be written to PPUADDR
   */
  @property void ppuData(ubyte f) {
    writeln("Wrote 0x" ~ format!"%x"(f) ~ " to: 0x"~format!"%x"(v));
    ppuram.write(v, f);
    v++;
  }
  @property ubyte ppuData() {
    return ppuram.read(v++);
  }

  /* OAM DMA $4014
    Writing $XX will upload 256 bytes of data from CPU page $XX00-$XXFF to
    internal PPU OAM. Typically located in internal RAM but can also be cartridge
  */
  @property void oamDMA(ubyte hi) {
    writeln("Uploading 256 bytes to PPU from 0x"~format!"%x"(hi));
    foreach(ubyte i; 0..0xFF) {
      ppuram.oamP[i] = proc.sysram.read(to!ubyte(hi<<0x08 + i));
    }
  }

  this() {
    //writeln(ppuScroll);
    ppuram = new PPUMemory();
    back = new Image();
    back.create(256, 240, Color(0, 0, 0));
    front = new Image();
    front.create(256, 240, Color(0, 0, 0));
  }

  void reset() {
    scanline = 240;
    dot = 340;
    fc = 0;
    w = 0;
    odd = false;
    x = 0;
    ctrl = 0;
    mask = 0;
    fstart = MonoTime.currTime;
  }

  void updateState() {
    if(scanline == 261 || scanline == -1) {
      state = PRERENDER;
    }else if(scanline < 240) {
      state = VISIBLE;
    }else if(scanline == 240) {
      state = POSTRENDER;
    }else {
      state = VBLANK;
      
    }
  }

  void tick() {
    infoP = "";
    updateState();
    if (state == VISIBLE || state == PRERENDER) {
      ubyte bg = backgroundPixel();
      if(bg >= 16 && bg%4 == 0) { //Some hack
          bg -= 16;
      }
      if(dot%340 < 256) {
        back.setPixel(dot, scanline, palette[bg]);
      }
      if(dot == 64) { //Clear Secondary OAM
        ppuram.clearoamS();
      }else if(dot == 256) { //Initialise secondary OAM for next scanline
        foreach(ushort i; 0..8) { //8  sprites per scanline
          foreach(ushort j; 0..63) { //64 sprites to search
            ubyte y = ppuram.readOAMP(to!ushort(4*j));
          }
        }
      }
    }
    if (state == VBLANK && dot == 1) {
      statusF.vBlank = 1;
      if (ctrlF.nmi) proc.set_nmi();
    }
    if(SHOW_PPU_ACTIVITY) {
      infoP ~= "PPU: ST:"~to!string(state)~" d :"~to!string(dot)~" y :"~to!string(scanline)~" t :"~to!string(t)~" v :"
      ~to!string(v)~"\n";
    }

    if(++dot == 340) { //End of scanline
      scanline++;
      dot = 0;
      v = 0;
      if(scanline == 261) {
        scanline = 0;
        statusF.vBlank = 0;
      }
    }
  }

  ubyte backgroundPixel() {
    if(!maskF.bg) {
      return 0;
    }
    tileLow = ppuram.read(v);
    v += 0x02;

    ubyte data = tileLow >> 7 & 0xFF; //Hack because no scrolling
    infoP ~= format!"%x"(tileLow) ~ " :: " ~ format!"%x"(data) ~ " \n";
    return data&0x0F;
  }

  ubyte read(ushort addr) { //For CPU register reads
    //writeln("Data read from PPU: "~to!string(addr));
    switch(addr) {
      case 0x02: //r
        return status;
      case 0x04: //wr
        return oamData;
      case 0x07: //wr
        return ppuData;
      default:
        return 0;
    }
  }
  void write(ushort addr, ubyte val) { //For CPU register writes
    writeln("Data written to PPU: "~format!"%x"(addr)~" # "~format!"%x"(val));
    statusF.bus = val&0b00011111;
    switch(addr) {
      case 0x00: //w
        ctrl = val;
        break;
      case 0x01: //w
        mask = val;
        break;
      case 0x03: //w
        oamAddr = val;
        break;
      case 0x04: //wr
        oamData = val;
        break;
      case 0x05: //ww
        ppuScroll = val;
        break;
      case 0x06: //ww
        ppuAddr = val;
        //writeln("ADDR:"~to!string(val));
        break;
      case 0x07: //wr
        ppuData = val;
        //writeln("DATA:"~to!string(val));
        break;
      default:
        break;
    }
  }
}

/+ Old tick
void storeTileData() {
    uint data;
    foreach(i; 0..8) {
      ubyte p1 = bgL&0x80 >> 7;
      ubyte p2 = bgH&0x80 >> 6;
      bgL <<= 1;
      bgH <<= 1;
      data |= cast(uint)(at | p1 | p2);
    }
    tileData |= cast(ulong)data;
    v++; //Test
  }

  void fetchNTByte() {
    ushort addr = 0x2000 | (v & 0x0FFF);
    nt = ppuram.read(addr);
  }

  void fetchATByte() {
    //Stolen line, reads the coords from v
    ushort addr = 0x23C0 | (v & 0x0C00) | ((v >> 4) & 0x38) | ((v >> 2) & 0x07);
    ushort shift = (((v >> 4) & 0x04) | v & 0x02);
    at = ((ppuram.read(addr) >> shift) & 0x03) << 2;
  }

  void fetchLTileByte() {
    ushort fineY = (v >> 12) & 7;
    ubyte table = ctrlF.bgTbl;
    ushort addr = to!ushort(0x1000*table + cast(ushort)table*16 + fineY);
    bgL = ppuram.read(addr);
  }

  void fetchHTileByte() {
    ushort fineY = (v >> 12) & 7;
    ubyte table = ctrlF.bgTbl;
    ushort addr = to!ushort(0x1000*table + cast(ushort)table*16 + fineY);
    bgH = ppuram.read(addr);
  }

  ubyte backgroundPixel() {
    if(!maskF.bg) {
      return 0;
    }
    ubyte data = tileData >> 39 & 0xFF; //Hack because no scrolling
    return data&0x0F;
  }

  void doFrame() { //Runs the PPU for one frame (Including vBlank for next frame) TODO

  }
switch(dot%8) {
      case 0:
        if(SHOW_PPU_ACTIVITY && infoP.split("\n").length < 6 && !lock) {
          infoP ~= "PPU: "~"F:"~to!string(fc)~"AT:"~to!string(at)~"NT:"~to!string(nt)~"BL:"~to!string(bgL)~"BH:"~to!string(bgH)~"TD:"~to!string(tileData)~"t :"~to!string(t)~"\n";
        }
   F.vBlank = 1;
        if (ctr     storeTileData();
        break;
      case 1:
        fetchNTByte();
        break;
      case 3:
        fetchATByte();
        break;
      case 5:
        fetchLTileByte();
        break;
      case 7:
        fetchHTileByte();
        break;
      default:
        break;
    }
    
    if(scanline >= 0) {
      if(dot < 256) { //Actually render
        /*x = dot;
          y = scanline;*/
        ubyte bg = backgroundPixel();
        if(bg >= 16 && bg%4 == 0) { //Some hack
          bg -= 16;
        }
        back.setPixel(dot, scanline, palette[bg]);
        //writeln(dot, " ~ ", scanline);
        
      }else if(dot == 256) {
        //Do scrolling stuff
      }else if(dot == 257 && render) {
        v = t;
      }else if(dot > 280 && dot <= 304) {

      }else if(dot > 328) {

      }
    }

    if(++dot > 340) {
      dot = 0;
      scanline++;
      if(scanline == 261) {
        scanline = odd?0:-1;
        odd = !odd;
        fc++;
        frames++;
        statusF.vBlank = 0;
        statusF.sprHit = 0;
        ftime = MonoTime.currTime-fstart;
        fstart = MonoTime.currTime;
        writeln("MS: ", ftime);

      }
    }
    if(scanline == 241) {
      if(dot == 1) {
        statusF.vBlank = 1;
        
      }
    }
+/