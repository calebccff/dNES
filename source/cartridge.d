import std.stdio;
import std.file;

import utils;
import memory;

/* File Map
    0-3: Constant $4E $45 $53 $1A ("NES" followed by MS-DOS end-of-file)
    4: Size of PRG ROM in 16 KB units
    5: Size of CHR ROM in 8 KB units (Value 0 means the board uses CHR RAM)
    6: Flags 6 - Mapper, mirroring, battery, trainer
    7: Flags 7 - Mapper, VS/Playchoice, NES 2.0
    8: Flags 8 - PRG-RAM size (rarely used extension)
    9: Flags 9 - TV system (rarely used extension)
    10: Flags 10 - TV system, PRG-RAM presence (unofficial, rarely used extension)
    11-15: Unused padding (should be filled with zero, but some rippers put their name across bytes 7-15)
*/

struct Cartridge{
  FileType type;
  ubyte[] prg;
  ubyte[] chr;

  void loadcart(CPUMemory ram){
    auto file = File("roms/nestest.nes", "r");
    auto header = file.rawRead(new ubyte[0x10]);
    bool iNESFormat=false;
    if (header[0]=='N' && header[1]=='E' && header[2]=='S' && header[3]==0x1A) { //"NES<EOF>"
      iNESFormat=true;
      type = iNES;
    }

    if (iNESFormat==true && (header[7]&0x0C)==0x08) { //Checks that bit 3 is set on the 7th byte
      type = NES20;
    }

    ubyte[] trainer;
    const bool hasTrainer = (header[6]&F6_TRAINER_P)==1;
    if(hasTrainer) {
      trainer = file.rawRead(new ubyte[0x200]);
    }

    ushort PRG = header[4];
    const ubyte  PRG_MSB = header[9]&F9_PRG_ROM<<8;
    ushort CHR = header[5];
    const ubyte  CHR_MSB = header[9]&F9_CHR_ROM<<4;

    ubyte mirror = header[6]&0x01; //Mirroring mode
    
    if(PRG_MSB == 0x0f)
    {
      //Exponent math
      writeln("STUB: Skipping PRG-ROM size calculation");
    } else
    {
      PRG += PRG_MSB; //Add MS 4 bits
    }
    if(CHR_MSB == 0x0f) // Repeat for Character ROM
    {
      //Exponent math
      writeln("STUB: Skipping CHR-ROM size calculation");
    } else
    {
      CHR += CHR_MSB; //Add MS 4 bits
    }
    write("PRG-ROM Bank count: ");
    writeln(PRG);
    write("CHR-ROM Bank count: ");
    writeln(CHR);

    /+ No offset as file pointer is always at last byte of previous read? +/
    //uint offset = hasTrainer?0x200:0;
    prg = file.rawRead(new ubyte[PRG*PRG_BANK_SIZE]);
    chr = file.rawRead(new ubyte[CHR*CHR_BANK_SIZE]);
    for(int i = 0; i < 0x8000; i++){
      ram.ram[0x8000+i] = prg[i%prg.length];
    }
  }

}