import std.stdio;
import std.conv;
import std.format;

import dsfml.graphics;
import dsfml.system;
import core.thread : Thread;

import cpu;
import p=ppu;
import memory;
import cartridge;

import global;

import vibe.web.rest;
import vibe.http.server, vibe.http.router;
import vibe.core.core : runApplication;

Thread render;
Thread system;
Thread info;
bool kill = false;

void main(){
  proc = new Mos6502();
  ppu = new p.NesPPU();
  cart.loadcart(proc.sysram); // Loads cartridge into RAM
  proc.reset();
  ppu.reset();
  
  render = new Thread(&renderThread);
  render.start();
  system = new Thread(&systemThread);
  system.start(); 

  info = new Thread(&infoWindowThread);
  info.start();

  //systemThread();
  
}

void infoWindowThread() {
  th.Thread.sleep(dur!("msecs")(50));
  RenderWindow details;
  details = new RenderWindow(VideoMode(1200, 1000), //VideoMode.getFullscreenModes()[0],
		"Debugger",
		Window.Style.None);
  Text t = new Text();
  Font font = new Font();
  font.loadFromFile("/usr/share/fonts/TTF/TerminusTTF.ttf");
  t.setFont(font);
  t.setColor = Color.White;
  t.setCharacterSize(16);

  while(details.isOpen()) {
    if(kill) Thread.yield();
    details.clear();
    t.setString(infoC);
    t.position = Vector2f(0, 0);
    details.draw(t);

    t.position = Vector2f(0, 50);
    t.setString(infoP);
    details.draw(t);

    for(ushort i = 0; i < 0x07FF; i++) {
      t.position = Vector2f((i*40)%1200, 100+i/(1200/40)*40);
      t.setString("0x"~format!"%x"(proc.sysram.read(i)));
      details.draw(t);
    }

    
    details.display();
  }
}

void renderThread() {
  RenderWindow window;
  dsfmlInitWindow(window);
  while (window.isOpen()) {
    if(kill) Thread.yield();
    Texture tex = new Texture();
    Sprite spr = new Sprite();
  
    Event event;
    while (window.pollEvent(event))
    {
        if (event.type == Event.EventType.Closed)
            window.close();
        else if(event.type == Event.EventType.KeyPressed) {
          switch(event.key.code) {
            case Keyboard.Key.S:
              doProcTick();
              break;
            case Keyboard.Key.Num1:
              foreach(i; 0..10) doProcTick();
              break;
            case Keyboard.Key.Num2:
              foreach(i; 0..20) doProcTick();
              break;
            case Keyboard.Key.Num3:
              foreach(i; 0..50) doProcTick();
              break;
            case Keyboard.Key.Num4:
              foreach(i; 0..100) doProcTick();
              break;
            default: break;
          }
        }
    }
    ppu.front.copyImage(ppu.back, 0, 0);
    tex.loadFromImage(ppu.front);
    //writeln("Got buffer");
    spr.setTexture(tex);
    //writeln("Set texture");
    spr.position = Vector2f(0, 0);
    spr.scale = Vector2f(width/256, width/256);
    window.clear(Color.Black);
    //writeln("Set position and scale");
    window.draw(spr);
    //writeln("Drew sprite");
    
    window.display();
    //writeln("FRAME: ", frameCount);
      
    frameCount++;
  }
}

void systemThread() {
  //71473200 is the number of CPU ticks per frame (ish)
  //sleep(dur!("msecs")(50));
  for(;;) {
    if(kill) Thread.yield();
    //sleep(dur!"seconds"(9999));
    //foreach(i; 0..1000){
      proc.tick();
      ppu.tick();
      ppu.tick();
      ppu.tick();
    //}
    //th.Thread.sleep(dur!("hnsecs")(50));
  }
}

void doProcTick() {
  proc.tick();
  ppu.tick();
  ppu.tick();
  ppu.tick();
}

shared static this() {
  XInitThreads();
}
shared static ~this() {
  kill = true;
  sleep(dur!"msecs"(20));
}

extern(C) void XInitThreads();
