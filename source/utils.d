import global;

import dsfml.graphics;
void dsfmlInitWindow(ref RenderWindow window) {
  import dsfml.graphics;
	window = new RenderWindow(VideoMode(width, height), //VideoMode.getFullscreenModes()[0],
		"Game",
		Window.Style.None);
  	window.setFramerateLimit(60);
}

enum FileType {
  iNES,
  NES20,
  INVALID,
}
alias iNES = FileType.iNES;
alias NES20 = FileType.NES20;
alias INVALID = FileType.INVALID;

// ROM Bank size
const uint PRG_BANK_SIZE   = 0x4000;
const uint CHR_BANK_SIZE   = 0x2000;

// Arbitrary bit masks
/+ Flag 6 +/
const ubyte F6_MIRROR_TYPE = 0b00000001;
const ubyte F6_BATTERY_P   = 0b00000010;
const ubyte F6_TRAINER_P   = 0b00000100;
const ubyte F6_FOURSCREEN  = 0b00001000;
const ubyte F6_MAPPER_NUM  = 0b11110000;

/+ Flag 9 +/
const ubyte F9_PRG_ROM     = 0b00001111;
const ubyte F9_CHR_ROM     = 0b11110000;

Color[64] palette;

static this() { //Initialise color palette
  int[] colors = [
    0x666666, 0x002A88, 0x1412A7, 0x3B00A4, 0x5C007E, 0x6E0040, 0x6C0600, 0x561D00,
		0x333500, 0x0B4800, 0x005200, 0x004F08, 0x00404D, 0x000000, 0x000000, 0x000000,
		0xADADAD, 0x155FD9, 0x4240FF, 0x7527FE, 0xA01ACC, 0xB71E7B, 0xB53120, 0x994E00,
		0x6B6D00, 0x388700, 0x0C9300, 0x008F32, 0x007C8D, 0x000000, 0x000000, 0x000000,
		0xFFFEFF, 0x64B0FF, 0x9290FF, 0xC676FF, 0xF36AFF, 0xFE6ECC, 0xFE8170, 0xEA9E22,
		0xBCBE00, 0x88D800, 0x5CE430, 0x45E082, 0x48CDDE, 0x4F4F4F, 0x000000, 0x000000,
		0xFFFEFF, 0xC0DFFF, 0xD3D2FF, 0xE8C8FF, 0xFBC2FF, 0xFEC4EA, 0xFECCC5, 0xF7D8A5,
    0xE4E594, 0xCFEF96, 0xBDF4AB, 0xB3F3CC, 0xB5EBF2, 0xB8B8B8, 0x000000, 0x000000,
  ];
  foreach(i, c; colors) {
    ubyte r = c >> 16 & 0xFF;
    ubyte g = c >> 8  & 0xFF;
    ubyte b = c       & 0xFF;
    palette[i] = Color(r, g, b);
  }
}