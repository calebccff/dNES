module global;

public import memory;
import cpu;
import cartridge;
import p=ppu;
public import utils;
public import core.stdc.stdlib;
public import th=core.thread;

__gshared Mos6502 proc;
__gshared p.NesPPU ppu;
__gshared Cartridge cart;

int width  = 600;
int height = 700;
int psize = 600/240;

const bool SHOW_CPU_INSTRUCTIONS = false;
const bool SHOW_PPU_ACTIVITY     = true;

__gshared string infoC;
__gshared string infoP;
__gshared bool lock = false;
__gshared int frameCount = 0;